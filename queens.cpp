#include "queens.h"
#include <iostream>
#include <stdlib.h>

void Board::solve(int row){

    for(int x=0; x<m_dimension; x++){
        if(insert_queen(x,row)){
            if(row == m_dimension-1){
                m_solutions++;
                this->print();
            }
            else{
                solve(row+1);
            }
            remove_queen(x,row);
        }
    }
    return;
}

void Board::solve(){
    solve(0);
}

bool Board::queen_crossed(int x, int y){
    if (m_board[x][y]==-1)
        return true;
    return false;
}

bool Board::insert_queen(int queen_x, int queen_y){
    for (int x=0; x<m_dimension; x++){
        for (int y=0; y<m_dimension; y++){
            if (x==queen_x || y==queen_y || x-y==queen_x-queen_y || x+y==queen_x+queen_y){
                if (!queen_crossed(x,y)){
                    m_board[x][y]+=1;
                }
                else{
                    return false;
                }
            }
        }
    }
    m_board[queen_x][queen_y]=-1;
    m_queens++;
      
    return true;
}

void Board::remove_queen(int queen_x, int queen_y){
    for (int x=0; x<m_dimension; x++){
        for (int y=0; y<m_dimension; y++){
            if (x==queen_x || y==queen_y || x-y==queen_x-queen_y || x+y==queen_x+queen_y){
                if (!queen_crossed(x,y)){
                    m_board[x][y]-=1;
                }
            }
        }
    }
    m_board[queen_x][queen_y]=0;
    m_queens--;
}

// a quite ugly finction
void Board::print() {
    for (int j=0; j<m_dimension; j++){
        std::cout<<"|";
        for(int i=0; i<m_dimension; i++){
            if (m_board[i][j]==0)
                std::cout<<" ";
            else if(m_board[i][j]==-1)
                std::cout<<"Q";
            else
                std::cout<<" ";
            std::cout<<"|";
        }
        std::cout<<std::endl;
    }    
    
    std::cout<<"Solution number "<< m_solutions << std::endl;
    std::cout<<std::endl;
}

int main(int argc, char* argv[]){
        
    int dimension = 8;
    if(argc == 2)
        dimension = atoi(argv[argc-1]);    
    
    Board board(dimension);
    board.solve();

    return 0;
}














