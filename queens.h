class Board{

private:
    int** m_board;
    int m_queens;
    int m_dimension;
    int m_solutions;

    bool queen_crossed(int x, int y);
    void remove_queen(int queen_x, int queen_y);

public:
    Board(int N){
        m_queens=0;
        m_solutions=0;
        m_dimension=N;
        
        m_board=new int*[m_dimension];
        for(int i=0; i<m_dimension; i++){
            m_board[i]=new int[m_dimension];
        }
        
        for (int i=0; i<m_dimension; i++)
            for (int j=0; j<m_dimension; j++)
                m_board[i][j]=0;    
    }

    ~Board(){
        for(int i=0; i<m_dimension; i++){
            delete[] m_board[i];
        }
        delete[] m_board;
    }
    
    bool insert_queen(int queen_x, int queen_y);
    void solve();
    void print();
    void solve(int row);
};
